from django.shortcuts import render, get_object_or_404, redirect
from post.models import Post, Comments, Hashtag, Category, Review, Users
from post.forms import PostForm, CommentForm, TagForm, ReviewForm
from django.db.models import F


def index(request):
    return redirect('categories')


def post_list(request):
    posts = Post.objects.filter(draft=False)
    return render(request, "post/post_list.html", {'posts': posts
                                                   })


def post_detail(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    Post.objects.filter(pk=post_pk).update(count=F('count') + 1)
    comments = Comments.objects.filter(post=post_pk)
    amount = len(comments)
    category = post.category
    reviews = Review.objects.filter(post=post_pk)
    estimate = 0
    if reviews:
        for i in reviews:
            estimate += i.mark
        post.rating = estimate / len(reviews)
        post.save()
    return render(request, 'post/post_detail.html', {'post': post,
                                                     'comments': comments,
                                                     'amount': amount,
                                                     'category': category,
                                                     'reviews': reviews
                                                     })


def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FIELS)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = PostForm()
        return render(request, 'post/post_new.html', {'form': form})


def post_edit(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'post/post_edit.html', {'form': form})


def post_delete(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.delete()
    return redirect('post_list')


def post_like(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    Post.objects.filter(pk=post_pk).update(likes=F('likes') + 1)
    return redirect('post_detail', post_pk=post.pk)


def post_dislike(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    Post.objects.filter(pk=post_pk).update(dislike=F('dislike') + 1)
    return redirect('post_detail', post_pk=post.pk)


def add_comment(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'post/comment_new.html', {'form': form})


def comment_delete(request, comment_pk, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    comment = get_object_or_404(Comments, pk=comment_pk)
    comment.delete()
    return redirect('post_detail', post_pk=post.pk)


def comment_edit(request, comment_pk, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    comment = get_object_or_404(Comments, pk=comment_pk)
    if request.method == "POST":
        form = CommentForm(request.POST, instance=comment)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = CommentForm(instance=comment)
    return render(request, 'post/comment_edit.html', {'form': form})


def tag(request, tag_pk):
    tag = get_object_or_404(Hashtag, pk=tag_pk)
    posts = tag.POSTS.filter()
    return render(request, "post/post_list.html", {'posts': posts})


def add_tag(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == "POST":
        form = TagForm(request.POST)
        if form.is_valid():
            tag = form.save(commit=False)
            tag.save()
            post.tag.add(tag)
            post.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = TagForm()
    return render(request, 'post/tag_new.html', {'form': form})


def publish(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.draft = False
    post.save()
    return redirect('post_detail', post_pk=post.pk)


def categories(request):
    categories = Category.objects.all()
    return render(request, 'post/categories.html', {'categories': categories})


def post_category(request, category_pk):
    posts = Post.objects.filter(category=category_pk)
    draft = Post.objects.filter(draft=True)
    count = len(draft)
    return render(request, "post/post_list.html", {'posts': posts,
                                                   'draft': draft,
                                                   'count': count})


def rating(request):
    posts = Post.objects.all()
    lst = []
    for post in posts:
        if post.rating > 4.5 and post.likes > 10:
            lst.append(post)
    return render(request, 'post/post_list.html', {'posts': lst})


def add_review(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save()
            review.mark = request.POST['mark']
            review.post = post
            review.author = request.user
            review.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = ReviewForm()
    return render(request, 'post/add_review.html', {'form': form})
