from django import forms
from post.models import Post, Comments, Hashtag, Review


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title',
                  'text',
                  'image',
                  'tag',
                  'draft',
                  ]


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = [
            'text'
        ]


class TagForm(forms.ModelForm):
    class Meta:
        model = Hashtag
        fields = [
            'text'
        ]


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            'text'
        ]
