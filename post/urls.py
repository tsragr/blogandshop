from django.urls import path
from post.views import post_list, post_detail, post_new, post_edit, post_delete, post_like, post_dislike, add_comment, \
    comment_delete, comment_edit, tag, add_tag, publish, categories, post_category, rating, add_review

urlpatterns = [
    path('new/', post_new, name='new'),
    path('list/', post_list, name='post_list'),
    path('detail/<int:post_pk>', post_detail, name='post_detail'),
    path('edit/<int:post_pk>', post_edit, name='edit'),
    path('delete/<int:post_pk>', post_delete, name='delete'),
    path('like/<int:post_pk>', post_like, name='post_like'),
    path('dislike/<int:post_pk>', post_dislike, name='post_dislike'),
    path('add_comment/<int:post_pk>/', add_comment, name='add_comment'),
    path('delete/<int:comment_pk>/<int:post_pk>', comment_delete, name='delete_comment'),
    path('edit/<int:comment_pk>/<int:post_pk>', comment_edit, name='edit_comment'),
    path('tag/<int:tag_pk>', tag, name='tag'),
    path('add_tag/<int:post_pk>', add_tag, name='add_tag'),
    path('publish/<int:post_pk>', publish, name='publish'),
    path('categories', categories, name='categories'),
    path('post_category/<int:category_pk>', post_category, name='post_category'),
    path('rating', rating, name='recommendations'),
    path('add_review/<int:post_pk>', add_review, name='add_review')
]
