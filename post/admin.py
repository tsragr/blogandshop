from django.contrib import admin

# Register your models here.
from post.models import Post, Comments, Hashtag, Category, Review, Users
from mptt.admin import MPTTModelAdmin

admin.site.register(Post)
admin.site.register(Comments)
admin.site.register(Hashtag)
admin.site.register(Category)
admin.site.register(Review)
admin.site.register(Users)