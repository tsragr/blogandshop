from django.db import models
from decimal import Decimal
from mptt.models import MPTTModel, TreeForeignKey


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT / post_<id>/<filename>
    return 'post_{0}/{1}'.format(instance.post.id, filename)


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=48)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    count = models.PositiveIntegerField(default=0)
    likes = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    image = models.ImageField(null=True, blank=True, upload_to='image', verbose_name='Image')
    tag = models.ManyToManyField('Hashtag', related_name='POSTS')
    draft = models.BooleanField(default=False)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, blank=True, null=True)
    rating = models.DecimalField(default=Decimal('3.00'),
                                 max_digits=3, decimal_places=2
                                 )

    def __str__(self):
        return f'{self.title}'

    @property
    def image_url(self):
        """
        Return self.photo.url if self.photo is not None,
        'url' exist and has a value, else, return None.
        """
        if self.image:
            return getattr(self.image, 'url')
        return None


class Comments(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True, null=True)
    text = models.TextField(max_length=228)
    date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, blank=True, null=True)


class Hashtag(models.Model):
    text = models.CharField(max_length=64)

    def __str__(self):
        return self.text


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Review(models.Model):
    mark = models.IntegerField(default=5)
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True, blank=True)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank=True)


class Users(models.Model):
    user = models.TextField()

    def __str__(self):
        return self.user

    def __repr__(self):
        return self.user
