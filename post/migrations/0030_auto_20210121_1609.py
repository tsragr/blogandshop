# Generated by Django 3.1.4 on 2021-01-21 13:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0029_auto_20210120_1924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='image', verbose_name='Image'),
        ),
    ]
