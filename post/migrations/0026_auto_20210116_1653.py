# Generated by Django 3.1.4 on 2021-01-16 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0025_auto_20210116_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='count',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='post',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='images/', verbose_name='Image'),
        ),
    ]
