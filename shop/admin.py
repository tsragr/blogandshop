from django.contrib import admin
from shop.models import Product, Category, Feedback, Basket, ProductInBasket, Favorite, ProductInFavorite

admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Feedback)
admin.site.register(Basket)
admin.site.register(ProductInBasket)
admin.site.register(Favorite)
admin.site.register(ProductInFavorite)
# Register your models here.
