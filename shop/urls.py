from django.urls import path
from shop.views import show_products, basket, create_product, product_detail, add_to_basket, delete_from_basket, \
    clear_basket, favorite

urlpatterns = [
    path('products/', show_products, name='products'),
    path('basket/', basket, name='basket'),
    path('create_product/', create_product, name='create_product'),
    path('<int:product_id>/', product_detail, name='detail'),
    path('add_to_basket/<int:product_pk>/', add_to_basket, name="add_to_basket"),
    path('delete_from_basket/<int:product_pk>/', delete_from_basket, name="delete_form_basket"),
    path('clear_basket/', clear_basket, name='clear_basket'),
    path('favorite/', favorite, name='favorite')
]
