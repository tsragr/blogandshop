from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from shop.models import Product, ProductInBasket, Basket, ProductInFavorite, Feedback
from shop.forms import ProductForm


def show_products(request):
    products = Product.objects.all()
    return render(request, 'shop/products.html', {'products': products})


def basket(request):
    products = ProductInBasket.objects.filter(basket__id=request.user.id)
    return render(request, 'shop/products.html', {'products_in_basket': products})


def create_product(request):
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('products')
    else:
        form = ProductForm()
    return render(request, 'shop/create_product.html', {'form': form})


def product_detail(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    if request.method == 'POST':
        Feedback.objects.create(product=product, text=request.POST['feedback'])
    feedbacks = Feedback.objects.filter(product=product)
    return render(request, 'shop/detail.html', {'product': product,
                                                'feedbacks': feedbacks})


def add_to_basket(request, product_pk):
    product = get_object_or_404(Product, pk=product_pk)
    basket = get_object_or_404(Basket, user=request.user)
    if ProductInBasket.objects.filter(product=product, basket=basket):
        product_in_basket = ProductInBasket.objects.get(product=product, basket=basket)
        product_in_basket.amount_of_product += request.POST['amount']
        product_in_basket.save()
    else:
        ProductInBasket.objects.create(product=product, basket=basket)
    return redirect('products')


def delete_from_basket(request, product_pk):
    product = get_object_or_404(Product, pk=product_pk)
    basket = get_object_or_404(Basket, user=request.user)
    product_to_delete = ProductInBasket.objects.get(product=product, basket=basket)
    product_to_delete.amount_of_product -= 1
    product_to_delete.save()
    if product_to_delete.amount_of_product == 0:
        product_to_delete.delete()
    return redirect("products")


def clear_basket(request):
    basket = get_object_or_404(Basket, user=request.user.id)
    products = ProductInBasket.objects.filter(basket=basket)
    for product in products:
        product.delete()
    return redirect("products")


def favorite(request):
    products = ProductInFavorite.objects.filter(favorite=request.user.id)
    return render(request, 'shop/products.html', {'products_in_favorite': products})
# Create your views here.
